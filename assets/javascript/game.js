// Code Game
// Javascript/jQuery

// Win/Loss/Score/Goal Counters
var wins = 0;
var losses = 0;
var score = 0;
var goal = 0;

// Start Game function
var start = function() {

// Clear the scoreboard
  $("#score").empty();
  $("#icons").empty();
  score = 0;

// Set the goal to a random number and display it inside #goal"
  goal = Math.floor((Math.random() * 100) + 20);
  $("#goal").text(goal);

// This game has four icons to choose from, so create four "<img>" places
  for (var i = 0; i < 4; i++) {

	var iconImage = $("<img>");

// Give each image a class
	iconImage.addClass("icon-image");

// Source a pic from the images folder
	iconImage.attr("src", "assets/images/hash.png");

// Give each icon a new attribute called icon-value
// icon-value = random number
	iconImage.attr("icon-value", Math.floor((Math.random() * 12) + 1));

// Append the image to #icons
	$("#icons").append(iconImage);

  };
}; // End of Start function

// Click events for each icon - Game Logic
$("#icons").on("click", ".icon-image", function () {

// Extract the icon-value and place it into iconValue
  var iconValue = ($(this).attr("icon-value"));

// Add it to the score
  iconValue = parseInt(iconValue);
  score += iconValue;

// Display the updated score in #score
  $("#score").text(score);

// If the score is equal to the goal, add +1 to wins
// Update #wins and restart the game
  if (score === goal) {
    wins++;
    $("#wins").text(wins);
    $("#result").text("You broke the code!")
    start();

// If the score goes over the goal, add +1 to losses
// Update #losses and restart the game.
  } else if (score > goal) {
    losses++;
    $("#losses").text(losses);
    $("#result").text("You went over, try again!")
    start();
  };

}); // End of click event

start(); // Run the game